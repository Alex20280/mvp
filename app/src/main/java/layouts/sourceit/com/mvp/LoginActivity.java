package layouts.sourceit.com.mvp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements MainMVP.view{

    Button tst;
    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new Presenter(this);

        tst = (Button) findViewById(R.id.toast);
        tst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.clickedTostButton();
            }
        });


    }


    @Override
    public void displayToastMessage(User user) {
        Toast.makeText(LoginActivity.this, "Hello There: " + user.getName(), Toast.LENGTH_LONG).show();
    }
}

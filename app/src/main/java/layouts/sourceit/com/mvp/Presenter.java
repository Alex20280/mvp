package layouts.sourceit.com.mvp;


public class Presenter implements MainMVP.presenter{

    private final MainMVP.view view;

    public Presenter(MainMVP.view view) {
        this.view = view;
    }

    @Override
    public void clickedTostButton() {
        User user = new User("Male", 78, "Alex");
        view.displayToastMessage(user);
    }

}
